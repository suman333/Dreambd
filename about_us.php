<?php include('header.php');?>
<?php include('left_sidebar.php');?>
<div id="content" class="col-sm-9">      <h1 class="cate_ttl">About Us</h1>
   <h4> At FaboTronix we manufacture PCB and design modules and antennas. Our company is founded by a group of engineers who wants to put their skills to practice to provide electronic solutions on demand. 

	Currently we are using screen printing and photoresist method to print PCBs for industry and students. We design a variety of modules such as RTC modules, LCD modules, Bread Board Power Supply modules, DC motor driver modules and others. We have component placement services too. 

	We started our company small with an intention to provide services to our customer with care, sincerity and honesty. We aspire to build upon our reputation and consequently introduce the latest technology to the local market and also set foot in the international market.

	We here at FaboTronix want to meet all your Electronics demand. </h4> 
</div>
<?php include('footer.php');?>